**Neural Networks**

---

In the repository all the scripts are written using Python 2.7. 

# Neural_Networks.py 

The Python script called Neural_Networks.py takes the custom input, true output data, sample names and sample clusters to predict outputs using Neural Networks.
The constructed feedforward neural networks model includes input layer, output layer and one hidden layer.

The program uses as an activation function ReLU except in the output layer where the sigmoid function is used. The loss calculation is done using the Binary Cross Entropy loss function. 

The program produces two performance measurement plots as outputs which are Receiver operating characteristic - Area Under Curve (ROC-AUC) and Matthews correlation coefficient (MCC) plots.

## Program options
-x --xdata = input x data

-y --ydata = output y data

-z --names = sample names

-c --cluster = path to the sample clusters

-s --split = number of CV splits

-r --random = random state

-n --normal = normalize the data

-l --learning = learning rate

-e --epochs = number of iterations

-d --hidden = number of neurons in the hidden layer

-h --help = show the help message (have fun!))

## Run the program

In order to run the RF_with_validation.py please follow the instructions:

Type on the command line:

python Neural_Networks.py -x /path/to/input_x -y /path/to/input_y -z /path/to/names -c /path/to/clusters -s [number] -r [number] -l [number] -e [number] -d [number] [options]

# Loss_plot.py

This program visualizes training and test data losses per iteration. The program fits the single or multiple output data.
The multi-output results are the avergae of all the losses. If one output among multiple outputs is desired to visualize, the user needs to feed to program with only the interested output.  

## Program options

-x --xdata = input x data

-y --ydata = output y data

-z --names = sample names

-c --cluster = path to the sample clusters

-s --split = number of CV splits

-r --random = random state

-n --normal = normalize the data

-l --learning = learning rate

-e --epochs = number of iterations

-d --hidden = number of neurons in the hidden layer

-h --help = show the help message (have fun!))

## Run the Program 

python Loss_plot.py -x /path/to/input -y /path/to/output -z /path/to/names -c /path/to/clusters -s [number] -r [number] -l [number] -e [number] -d [number] [options]

# Raw2NN.py

This program does all the analyses required by Neural_Networks.py, afterwards, runs the neural networks program to predict antimicrobial resistance profiles. 

The program should be fed by the raw sequeneces with .fasta and .fastq extensions (.fna files should be renamed). 

The program uses all the required data preparation and neural networks programs that are available in the data_preparation and Neural_Networks reciprocities, therefore, those needs to be downloaded. 
Ideally, create a directory typing on the terminal "mkdir test_folder" then download all the reciprocities inside the generated folder after typing "cd test_folder".

## Program options

-i --input = Input data folder inclusing .fasta or .fastq files

-y --data_y = True output values

-p --PathPoint = Path to the PointFinder program

-d --PointData = Path to the PointFinder database

-s --species = Species name (should be same with pointfinder database options)

-k --kma = Path to kma program

-r --ResFinder = Path to the ResFinder program

-n --nn_path = Path to the Neural_Networks.py program

-a --spa_path = Path to SPAdes

-c --kma_clust = Path to the kma_clustering.c program

-f --scaffolds = Path to the merge_scaffolds.py program

-g --point_prog = Path to the PointFinder analyser program - you shall choose one of four of them

-m --mut = If user choose to scored_representation.py, merge the binary representation with scored representation

-e --res_prog = Path to the ResFinder analyser program

-R --merge_results = Path to the merge_resfinder_pointfinder.py program

-I --merge_inout = Path to the merge_input_output_files.py

-h --help = Show the help message (have fun!))

## Run the program 

python Raw2NN.py -i /path/to/input/folder -y /path/to/outputs -p /path/to/PointFinder/program -d /path/to/PointFinder/database -s [name] -k /path/to/kma/program -r /path/to/ResFinder/program -n /path/to/Neural_Networks.py -a /path/to/SPAdes/program -c /path/to/kma_clustering -f /path/to/merge_scaffolds.py -g /path/to/binary_representation.py -e /path/to/ResFinder_analyser.py -R /path/to/merge_resfinder_pointfinder.py -I /path/to/merge_input_output_files.py [options]

# pre_trained_nn.py

This program uses pretrained models to predict antimicrobial resistance profiles of unseen datasets. 

There are two models saved, the first model was trained by distinct databases and the second model by a merged dataset.
The models were trained with the datasets include for different species namely E. coli, M. tuberculosis, S. enterica and S. aureus to predict six different antimicrobial resistance profiles Rifampicin, Isoniazid, Streptomycin, Ethambutol, Pyrazinamide and Ciprofloxacin. 
The trained models have 200 hidden neurons in the hidden layer. The new datasets should have the same dimensions with the data used for training. 

Before run the program, model weights (available in the Neural_Networks folder) should be downloaded to the folder the script is downloaded. 

Also, there are two validation files are available in Downloads. These validation files include multiple species and 
multiple-outputs, validation_merged_x.txt and validation_merged_y.txt produced by the concatenated reference genomes, validation_separated_x.txt and validation_separated_y.txt by the distinct reference genomes. These validation files are the same with used in the Aytan-Aktug et al.(2019) paper. 

## Program options

-x --xdata = input x data

-y --ydata = output y data

-d --distinct = loads the model generated using the distict databases

-m --merged = loads the model generated using the merged databases

-h --help = show the help message (have fun!)

## Run the program

python pre_trained_nn.py -x /path/to/input -y /path/to/output -d|-m

# pre_trained_nn_per_species.py

This program shares the same objectives with the pre_trained_nn.py program, however, it can provide AUC performances for only the samples interested instead of all. Additionaly, it provides sensitivity, specificity and F-1 score. 

## Program options

-x --xdata = input x data

-y --ydata = output y data

-s --start = the first index of the interested data

-f --final = the last index of the interested data

-d --distinct = loads the model generated using the distict databases

-m --merged = loads the model generated using the merged databases

-h --help = show the help message (have fun!)

## Run the program

python pre_trained_nn.py -x /path/to/input -y /path/to/output -s [number] -f [number] -d|-m

## Raw2NN_pretrained.py

This program basically synthesis the Raw2NN.py and pre_trained_nn.py programs. It takes all the raw data as Raw2NN.py does, and prepare the raw datasets to the Neural Networks program.
The difference between the Raw2NN.py and Raw2NN_pretrained.py programs is that the Raw2NN_pretrained.py program uses the pretrained Neural Networks model as pre_trained_nn.py does.

Please not that as it is required for the pre_trained_nn.py program, the new dataset should have the same dimensions with the already trained Neural Networks model. 

## Program options 

-i --input = Input data folder inclusing .fasta or .fastq files

-y --data_y = True output values

-p --PathPoint = Path to the PointFinder program

-d --PointData = Path to the PointFinder database

-s --species = Species name (should be same with pointfinder database options)

-k --kma = Path to kma program

-r --ResFinder = Path to the ResFinder program

-n --nn_path = Path to the pre_trained_nn.py program

-g --point_prog = Path to the PointFinder analyser program - you shall choose one of four of them

-u --mut = If user choose to scored_representation.py, merge the binary representation with scored representation

-e --res_prog = Path to the ResFinder analyser program

-R --merge_results = Path to the merge_resfinder_pointfinder.py program

-I --merge_inout = Path to the merge_input_output_files.py

-m --merged_database = use the model weights trained by the samples aligned to the merged database

-c --distinct_database = use the model weights trained by the samples aligned to the distinct databases

-h --help = Show the help message (have fun!)

## Run the program

python Raw2NN.py -i /path/to/input/folder -y /path/to/outputs -p /path/to/PointFinder/program -d /path/to/PointFinder/database -s [name] -k /path/to/kma/program -r /path/to/ResFinder/program -n /path/to/pre_trained_nn.py -g /path/to/binary_representation.py -e /path/to/ResFinder_analyser.py -R /path/to/merge_resfinder_pointfinder.py -I /path/to/merge_input_output_files.py -c|-m [options]

# pre_trained_nn_per_species.py

This program shares the same objectives with the pre_trained_nn.py program, however, it can provide AUC performances for only the samples interested instead of all. Additionaly, it provides sensitivity, specificity and F-1 score. 

## Program options

-x --xdata = input x data

-y --ydata = output y data

-s --start = the first index of the interested data

-f --final = the last index of the interested data

-d --distinct = loads the model generated using the distict databases

-m --merged = loads the model generated using the merged databases

-h --help = show the help message (have fun!)

## Run the program

python pre_trained_nn.py -x /path/to/input -y /path/to/output -s [number] -f [number] -d|-m

# NN_feature_importance.py

This program finds the important features for the neural networks model using the Shap package. The program requires additionally a file including feature names and a number of background samples. It is highly recommended to optimize the number of background samples. 

## Program options 

-x --xdata = input x data

-y --ydata = output y data

-z --names = sample names

-f --features = data feature names

-c --cluster = path to the sample clusters

-s --split = number of CV splits

-r --random = random state

-n --normal = normalize the data

-l --learning = learning rate

-e --epochs = number of iterations

-d --hidden = number of neurons in the hidden layer

-b --background_n = number of background samples

-h --help = show the help message (have fun!))

## Run the program

python NN_feature_importance.py -x /path/to/input_x -y /path/to/input_y -z /path/to/names -f /path/to/features -c /path/to/clusters -s [number] -r [number] -l [number] -e [number] -d [number] -b [number][options]

# Dependencies 

## Downloading and compiling 
## SPAdes

wget http://cab.spbu.ru/files/release3.12.0/SPAdes-3.12.0.tar.gz


tar -xzf SPAdes-3.12.0.tar.gz


cd SPAdes-3.12.0


./spades_compile.sh    

## KMA

git clone https://bitbucket.org/genomicepidemiology/kma.git

cd kma && make

mv kma ~/bin/

## kma_clustering

git clone https://deaytan@bitbucket.org/deaytan/data-preparation.git

gcc -O3 -o kma_clustering kma_clustering.c -lm -lz

## ResFinder

git clone https://git@bitbucket.org/genomicepidemiology/resfinder.git

git clone https://git@bitbucket.org/genomicepidemiology/resfinder_db.git

ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/LATEST/

pip3 install cgecore

## PointFinder 

git clone https://bitbucket.org/genomicepidemiology/pointfinder_db.git

## Required Python Packages

* getopt
* sys
* warnings
* matplotlib
* torch
* numpy
* sklearn
* pandas
* scipy
* collections
* random
* os
* re
* Bio
* Shap 