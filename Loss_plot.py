#!/usr/bin/python2.7 
import getopt, sys
import warnings

if not sys.warnoptions:
    warnings.simplefilter("ignore")

##read commandline arguments first 
fullCmdArguments = sys.argv
##further argumnets 
argumentList = fullCmdArguments[1:]
#print(argumentList)

unixOptions = "x:y:z:c:s:r:e:d:l:nh"
gnuOptions = ["xdata=", "ydata=", "names=", "clusters=", "split=", "random=", "epochs=", "hidden=", "learning=", "normal","help"]


try:
	arguments, values = getopt.getopt(argumentList, unixOptions, gnuOptions)
except getopt.error as err:
	print(str(err))
	sys.exit(2)

for currentArgument, currentValue in arguments:
	if currentArgument in ("-h", "--help"):
		print("-x --xdata = input x data")
		print("-y --ydata = output y data") 
		print("-z --names = sample names")
		print("-c --cluster = path to the sample clusters")
		print("-s --split = number of CV splits")
		print("-r --random = random state")
		print("-n --normal = normalize the data")
		print("-l --learning = learning rate")
		print("-e --epochs = number of iterations")
		print("-d --hidden = number of neurons in the hidden layer")
		print("-h --help = show the help message (have fun!))")
		sys.exit()
		


def main():

	##import the required packages 
	import matplotlib
	matplotlib.use('Agg')
	import torch
	import torch.nn as nn
	import numpy as np
	import torch.optim as optim
	from torch.autograd import Variable
	from sklearn.model_selection import KFold
	from sklearn.metrics import matthews_corrcoef
	import pandas
	import matplotlib.pyplot as plt
	from sklearn.metrics import roc_curve, auc
	from scipy import interp
	import collections
	import random
	import torch.nn.functional as F
	from torch.utils import data
	from sklearn import utils
	from sklearn import preprocessing

	for currentArgument, currentValue in arguments:
		if currentArgument in ("-x","--xdata"):
			data_x_all = np.loadtxt("%s" % currentValue, dtype="float")
		if currentArgument in ("-y","--ydata"):
			data_y_all = np.loadtxt("%s" % currentValue)
		if currentArgument in ("-z", "--names"):
			names_open = open("%s" % currentValue, "r")
			names_read = names_open.readlines()
		if currentArgument in ("-c", "--clusters"):
			output_file_open = open("%s" % currentValue, "r")
			output_file = output_file_open.readlines()
		if currentArgument in ("-s", "--split"):
			cv = int(currentValue)
		if currentArgument in ("-r", "--random"):
			Random_State = int(currentValue)
		if currentArgument in ("-e", "--epochs"):
			n_epochs = int(currentValue)
		if currentArgument in ("-d", "--hidden"):
			n_hidden = int(currentValue)
		if currentArgument in ("-l", "--learning"):
			learning_rate = float(currentValue)

	class _classifier(nn.Module):
	    def __init__(self, nlabel):
		super(_classifier, self).__init__()
		self.main = nn.Sequential(
		    nn.Linear(D_in, H),
		    nn.ReLU(),
		    nn.Linear(H, nlabel),
		)

	    def forward(self, input):
		return self.main(input)

	dict_cluster = collections.defaultdict(list)

	for each in output_file:
		splitted = each.split()
		if splitted != []:
			if splitted[0].isdigit():
				dict_cluster[str(int(splitted[0])-1)].append(splitted[3])
			if splitted[0] == "Similar":
				splitted = each.split()
				splitted_2 = each.split(":")
				dict_cluster[str(int(splitted_2[1].split()[0])-1)].append(splitted[6])

	keys = sorted(dict_cluster.keys())

	##find the sample names
	names_all = []
	for each in range(len(names_read)): #ecoli_sal:
		names_all.append(names_read[each].replace("\n", ""))

	##seperate some of the samples as validation data:
	all_samples = range(0, len(data_y_all))

	desired_val = len(all_samples)/6

	desired_keys = range(1, len(keys)+1, 6)

	remained_keys = list(set(range(1, len(keys)+1)) - set(desired_keys))

	validation_names = [] 
	for i in desired_keys:
		validation_names.append(dict_cluster[str(i)])	

	final_keys = list(set(range(1, len(keys)+1)) - set(desired_keys))	

	validation = []
	for each in desired_keys:
		for item in dict_cluster[str(each)]:
			if item in names_all:
				validation.append(names_all.index(item))
	remained = list(set(all_samples) - set(validation))

	##validation data
	validation_x = data_x_all[validation]
	validation_y = data_y_all[validation]

	##remaining data
	data_x = data_x_all[remained]
	data_y = data_y_all[remained]

	##subtract the samples from the names as well:
	names = []
	for e in range(0, len(names_all)):
		if e not in validation:
			names.append(names_all[e])

	###construct the Artificial Neural Networks Model###
	##The feed forward NN has only one hidden layer 
	##The activation function used in the input and hidden layer is ReLU, in the output layer the sigmoid function. 
			
	H = n_hidden ###number of neurons in the hidden layer 

	D_in = len(data_x[0]) ##number of neurons in the input layer 
	N = len(data_x) ##number of input samples
	nlabel = data_y[0].size ## number of neurons in the output layer 
	epochs = n_epochs ## number of iterations the each model will be trained

	mean_fpr = np.linspace(0, 1, 100)

	##Custom k fold cross validation
	##cross validation method divides the clusters and adds to the partitions. 
	##Samples are not divided directly due to sample similarity issue
	all_data_splits_pre = []
	all_available_data = range(0,len(dict_cluster)) ## all the clusters had
	clusters_n = len(dict_cluster) ##number of clusters 

	all_samples = [] ## all the samples had in the clusters
	for i in dict_cluster:
		for each in dict_cluster[i]:
			all_samples.append(each)

	##Shuffle the clusters and divide them
	shuffled = list(utils.shuffle(dict_cluster.keys(), random_state = Random_State)) ##shuffled cluster names 

	##Divide the clusters equally 
	r = int(len(shuffled)/cv) ## batches 

	a = 0
	b = r
	for i in range(cv):

		all_data_splits_pre.append(shuffled[a:b])
		
		if i != cv -2:
			a = b
			b = b + r
		
		else:
			a = b
			b = len(shuffled)

	##Extract the samples inside the clusters 
	##If the number of samples are lower than the expected number of samples per partition, get an extra cluster
	all_data_splits = []
	totals = []
	all_extra = []
	for i in range(len(all_data_splits_pre)):
		if i == 0:
			m = i + 1
		else:
			m = np.argmax(totals)
		tem = []
		extracted = list(set(all_data_splits_pre[i])-set(all_extra))

		for e in extracted:	
			elements = dict_cluster[str(e)]
			tem.append(len(elements))
		sum_tem = sum(tem)
		
		if sum_tem + 200 < len(all_samples)/float(cv):
			a = 0
			while sum_tem + 200 < len(all_samples)/float(cv):
				extra = list(utils.shuffle(all_data_splits_pre[m], random_state = Random_State))[a]
				extracted = extracted + [extra]
				all_extra.append(extra)
				a = a + 1
				tem = []
				for e in extracted:	
					elements = dict_cluster[str(e)]
					tem.append(len(elements))
				sum_tem = sum(tem)
				for item in range(len(all_data_splits)):
					all_data_splits[item] = list(set(all_data_splits[item]) - set([extra]))
		totals.append(sum_tem)	
		all_data_splits.append(extracted)
					

	##cross validation loop where the training and testing performed. 
	all_losses_test = []
	all_losses_train = []
	m = nn.Sigmoid() ##sigmoid function for the ooutput layer
	for cc in range(cv):
		temp_test = []
		temp_train = []
		test_clusters = all_data_splits[cc] ## clusters including test data 
		remain = list(set(range(cv)) - set([cc])) ## clusters including validation data 
		training_clusters = []
		for e in remain:
			for ee in all_data_splits[e]:
				training_clusters.append(ee)

		###extract the training data indexes:
		train_samples = []
		for each in training_clusters:
			for element in dict_cluster["%s" %each]:
				if element in names:
					train_samples.append(names.index(element))
				if element in names_all:
					ind = names_all.index(element)

		
		###extract the test data indexes:
		test_samples = []
		for each in test_clusters:
			for element in dict_cluster["%s" %each]:
				if element in names:
					test_samples.append(names.index(element))
				if element in names_all:
					ind = names_all.index(element)
					
		##training and test samples 			
		x_train, x_test = data_x[train_samples], data_x[test_samples]
		y_train, y_test = data_y[train_samples], data_y[test_samples]	

		##if user chose to normalize the data 

		for currentArgument, currentValue in arguments:
			if currentArgument in ("-n", "--normal"):
				scaler = preprocessing.StandardScaler().fit(x_train)
				x_train = scaler.transform(x_train)
				x_test = scaler.transform(x_test) ##scale the test data based on the training data 
				validation_x = scaler.transform(validation_x_raw) ##scale the validation data based on the training data

		##generate a NN model 
		classifier = _classifier(nlabel)
		##generate the optimizer - Stochastic Gradient Descent
		##learning rate = 0.001
		optimizer = optim.SGD(classifier.parameters(), lr = learning_rate)

		##iterate the loop for number of epochs 
		for epoc in range(epochs):
			x_train_new = torch.utils.data.TensorDataset(torch.from_numpy(x_train).float())
			y_train_new = torch.utils.data.TensorDataset(torch.from_numpy(y_train).float())
			all_data = zip(x_train_new, y_train_new)
			
			## the model is trained for 100 batches
			data_loader = torch.utils.data.DataLoader(all_data, batch_size=100, drop_last = False)
			
			losses = [] ## save the error for each iteration
			for i, (sample_x, sample_y) in enumerate(data_loader):
					inputv = sample_x[0]
					inputv = Variable(torch.FloatTensor(inputv)).view(len(inputv),-1)
	
					if nlabel == 1:
						labelsv = sample_y[0]
					else:
						labelsv = sample_y[0][:,:]
					weights = labelsv.data.clone()
					
					##That step is added to handle missing outputs. 	
					##Weights are not updated in the prsence of a missing value. 
					weights[weights == 1.0] = 1
					weights[weights == 0.0] = 1
					weights[weights < 0] = 0
					
					##Calculate the loss/error using Binary Cross Entropy Loss 				
					criterion = nn.BCELoss(weight= weights, reduction= "none")
					output = classifier(inputv)
					loss = criterion(m(output), labelsv)
					loss = loss.mean() #compute loss
					optimizer.zero_grad() #zero gradients #previous gradients do not keep accumulating 
					loss.backward() #backpropagation
					optimizer.step() #weights updated 
					losses.append(loss.data.mean())
			##predictions for the test data
			losses_test = []
			for a, a_sample in enumerate(x_test):
				tested = Variable(torch.FloatTensor(a_sample)).view(1,-1)
				if nlabel == 1:
					tested_y = Variable(torch.FloatTensor([y_test[a]])).view(1,-1)
				else:
					tested_y = Variable(torch.FloatTensor(y_test[a])).view(1,-1)
				output_test = classifier(tested)
				weights_test = tested_y.data.clone()

				weights_test[weights_test == 1.0] = 1
				weights_test[weights_test == 0.0] = 1
				weights_test[weights_test < 0] = 0
					
					
				criterion_test = nn.BCELoss(weight= weights_test, reduction= "none")
				output_test = classifier(tested)
				loss_test = criterion_test(m(output_test), tested_y)
				loss_test = loss_test.mean()
				losses_test.append(loss_test.data.mean())
			temp_test.append(np.mean(losses_test))
			temp_train.append(np.mean(losses))
		all_losses_test.append(temp_test)
		all_losses_train.append(temp_train)
	test_res = np.sum(all_losses_test, axis = 0)/float(cv)
	train_res = np.sum(all_losses_train, axis = 0) / float(cv)

	plt.plot(list(test_res), color = "red", label="test data")
	plt.plot(list(train_res), color = "blue", label = "train data")
	plt.xlabel("number of epochs")
	plt.ylabel("Losses")
	plt.legend()
	plt.savefig("loss_plot.png")
	plt.close()	

if __name__ == '__main__':
    main()		