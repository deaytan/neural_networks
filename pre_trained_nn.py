#!/usr/bin/python2.7
##import the required packages 
import getopt, sys
import warnings

if not sys.warnoptions:
    warnings.simplefilter("ignore")

##read commandline arguments first 
fullCmdArguments = sys.argv
##further argumnets 
argumentList = fullCmdArguments[1:]
#print(argumentList)

unixOptions = "x:y:dmh"
gnuOptions = ["xdata=", "ydata=", "distinct", "merged", "help"]


try:
	arguments, values = getopt.getopt(argumentList, unixOptions, gnuOptions)
except getopt.error as err:
	print(str(err))
	sys.exit(2)

for currentArgument, currentValue in arguments:
	if currentArgument in ("-h", "--help"):
		print("-x --xdata = input x data")
		print("-y --ydata = input y data") 
		print("-d --distinct = loads the model generated using the distict databases")
		print("-m --merged = loads the model generated using the merged databases")
		print("-h --help = show the help message (have fun!)")
		sys.exit()


def main():
	import matplotlib
	matplotlib.use('Agg')
	import torch
	import torch.nn as nn
	import numpy as np
	import torch.optim as optim
	from torch.autograd import Variable
	from sklearn.metrics import matthews_corrcoef
	import matplotlib.pyplot as plt
	from sklearn.metrics import roc_curve, auc
	from scipy import interp
	from torch.utils import data
	from sklearn import utils

	class _classifier(nn.Module):
		def __init__(self, nlabel):
			super(_classifier, self).__init__()
			self.main = nn.Sequential(
				nn.Linear(D_in, H),
				nn.ReLU(),
				nn.Linear(H, nlabel),
			)

		def forward(self, input):
			return self.main(input)

	for currentArgument, currentValue in arguments:
		if currentArgument in ("-x","--xdata"):
			data_x_all = (np.loadtxt("%s" % currentValue, dtype="float"))
		if currentArgument in ("-y","--ydata"):
			data_y_all = (np.loadtxt("%s" % currentValue))

	f_n = data_y_all[0].size ##number of features 

	###construct the Artificial Neural Networks Model###
	##The feed forward NN has only one hidden layer 
	##The activation function used in the input and hidden layer is ReLU, in the output layer the sigmoid function. 

	for currentArgument, currentValue in arguments:
		if currentArgument in ("-d","--distinct"):
			weight_list = ["sep_multiple_weights0", "sep_multiple_weights1", "sep_multiple_weights2", "sep_multiple_weights3", "sep_multiple_weights4"]

	for currentArgument, currentValue in arguments:
		if currentArgument in ("-m","--merged"):
			weight_list = ["mer_multi_weights0", "mer_multi_weights1", "mer_multi_weights2", "mer_multi_weights3", "mer_multi_weights4"]


	##prepare data stores##

	pred_val_all = [] ##probabilities are for the validation set
					
	m = nn.Sigmoid() ##sigmoid function for the output layer

	colors = ["darkblue", "darkred", "darkgreen", "orange", "purple", "magenta"]*100
	legends = range(1, 501)

	for weight in weight_list: 

		
		for currentArgument, currentValue in arguments:
			if currentArgument in ("-d","--distinct"):
				
				H = 200 ## number of neurons in the hidden layer 
				D_in = len(data_x_all[0]) ## number of neurons in the input layer 
				nlabel = data_y_all[0].size  ## number of neurons in the output layer 

				##generate a NN model 
				model = _classifier(nlabel)

		for currentArgument, currentValue in arguments:
			if currentArgument in ("-m","--merged"):

				H = 200 
				D_in = len(data_x_all[0]) 
				nlabel = data_y_all[0].size  

				##generate a NN model 
				model = _classifier(nlabel)

		model.load_state_dict(torch.load(weight))

		##apply the trained model to the validation data 
		pred_val = []
		for v, v_sample in enumerate(data_x_all):
			val = Variable(torch.FloatTensor(v_sample)).view(1,-1)
			output_test = model(val) 
			out = m(output_test) 
			temp = []
			for h in out[0]:
				temp.append(float(h))
			pred_val.append(temp)
		pred_val = np.array(pred_val)
		pred_val_all.append(pred_val)

	##Calculate the MCC for the validation data, for the same thresholds 
	all_thresholds_valid = []
	for threshold in np.arange(0, 1.1, 0.1):
		pred_validation = []
		for r in range(0, len(data_x_all)):
			all_predictions = []
			for item in pred_val_all:
				all_predictions.append(item[r])
			all_predictions_all = np.array(all_predictions).sum(axis=0)/float(len(weight_list)) #take the average of the probabilities
			temp = [] 
			for c in all_predictions_all:
				if c > threshold:
					temp.append(1)
				else:
					temp.append(0)
			
			pred_validation.append(temp)
		pred_validation = np.array(pred_validation)

		##mcc values for the validation data 
		##do not take into consideration missing outputs. 
		mcc_all = []
		for i in range(f_n): 
			comp = []
			for t in range(len(data_y_all)): 
				if f_n == 1:
					if -1 != data_y_all[t]:
						comp.append(t)
				else:
					if -1 != data_y_all[t][i]:
						comp.append(t)
			y_val_sub = data_y_all[comp]
			pred_sub_val = pred_validation[comp]
			if f_n == 1:
				mcc = matthews_corrcoef(y_val_sub, pred_sub_val)
			else:
				mcc = matthews_corrcoef(y_val_sub[:,i], pred_sub_val[:,i])
			mcc_all.append(mcc)
		all_thresholds_valid.append(mcc_all)

	##plot MCC values per output 
	for i in range(f_n):
		aver_all_mcc = []
		for val in all_thresholds_valid:
			if f_n == 1:
				aver_all_mcc.append(val)
			else:
				aver_all_mcc.append(val[i])
		plt.plot(np.arange(0,1.1,0.1),aver_all_mcc, color = colors[i], alpha = 1, label = legends[i])
	plt.xlim([0,1])
	plt.xlabel("Thresholds")
	plt.ylabel("MCC average values for %s fold CV" % str(len(weight_list)))
	plt.title("Thresholds vs MCC values")
	plt.legend(loc="best")
	plt.savefig("./MCC_output_validation.png")
	plt.close()


	###plot the AUC values 
	plt.figure(figsize=(8, 8)) ##figure size 
	pred_val_all = np.sum(pred_val_all, axis = 0)/float(len(weight_list)) ##take the average of the validation predictions

	##calculate the AUCs for the validation data
	##do not take into consideration missing outputs 
	for i in range(f_n):
		comp = []
		for t in range(len(data_y_all)):
			if f_n == 1:
				if -1 != data_y_all[t] or -1.0 != data_y_all[t]:
					comp.append(t)
			else:
				if -1 != data_y_all[t][i] or -1.0 != data_y_all[t][i]:
					comp.append(t)
		y_val_sub = data_y_all[comp]
		pred_sub_val = pred_val_all[comp]
		if f_n == 1:
			fpr1, tpr1, _ = roc_curve(y_val_sub, pred_sub_val, pos_label = 1)
		else:
			fpr1, tpr1, _ = roc_curve(y_val_sub[:,i], pred_sub_val[:,i], pos_label = 1)
		roc_auc1 = auc(fpr1, tpr1)
		plt.plot(fpr1, tpr1, color=colors[i], alpha = 1, lw=1, label='ROC curve for validation (area = %0.2f)' % roc_auc1)	

	plt.plot([0, 1], [0, 1], linestyle='--', lw=2, color='r', label='Chance', alpha=.8)
	plt.xlim([-0.05, 1.05])
	plt.ylim([-0.05, 1.05])
	plt.xlabel('False Positive Rate')
	plt.ylabel('True Positive Rate')
	plt.title("ROC Curve")
	plt.legend(loc="lower right")
	plt.savefig("./ROC_curve.png")
	plt.close()


if __name__ == '__main__':
    main()