#!/usr/bin/python2.7 
import getopt, sys
import warnings

if not sys.warnoptions:
    warnings.simplefilter("ignore")

##read commandline arguments first 
fullCmdArguments = sys.argv
##further argumnets 
argumentList = fullCmdArguments[1:]
#print(argumentList)

unixOptions = "i:p:d:s:k:r:y:n:g:e:R:I:umch"
gnuOptions = ["input=", "PathPoint=", "PointData=", "species=", "kma=", "ResFinder=", "data_y=", "nn_path=","point_prog=", "res_prog=", "merge_results=", "merge_inout=", "mut", "merged_database", "distinct_database", "help"]


try:
	arguments, values = getopt.getopt(argumentList, unixOptions, gnuOptions)
except getopt.error as err:
	print(str(err))
	sys.exit(2)

for currentArgument, currentValue in arguments:
	if currentArgument in ("-h", "--help"):
		print("-i --input = Input data folder inclusing .fasta or .fastq files")
		print("-y --data_y = True output values")
		print("-p --PathPoint = Path to the PointFinder program")
		print("-d --PointData = Path to the PointFinder database")
		print("-s --species = Species name (should be same with pointfinder database options)")
		print("-k --kma = Path to kma program")
		print("-r --ResFinder = Path to the ResFinder program")
		print("-n --nn_path = Path to the pre_trained_nn.py program")
		print("-g --point_prog = Path to the PointFinder analyser program - you shall choose one of four of them")
		print("-u --mut = If user choose to scored_representation.py, merge the binary representation with scored representation")
		print("-e --res_prog = Path to the ResFinder analyser program")
		print("-R --merge_results = Path to the merge_resfinder_pointfinder.py program")
		print("-I --merge_inout = Path to the merge_input_output_files.py")
		print("-m --merged_database = use the model weights trained by the samples aligned to the merged database")
		print("-c --distinct_database = use the model weights trained by the samples aligned to the distinct databases")
		print("-h --help = Show the help message (have fun!)")
		sys.exit()
		


def main():

	###this script aims to run all the processes from the beginning to the end. 

	###Steps###
	##Run pointfinder
	##Run ResFinder
	##Run clustering program 
	##prepare the data
	##prepare the outputs 
	##run the machine learning model
	##visualize the results

	#####Packages needs to be imported#####

	import os
	import numpy as np

	###write defaults###

	path_to_nn = ("./Neural_Networks/Neural_networks.py")
	path_to_kma_clust = ("./data_preparation/kma_clustering.c")
	path_to_scaffolds = ("./data_preparation/merge_scaffolds.py")
	path_to_pointfinder_prog = ("./data_preparation/scored_representation.py")
	path_to_resfinder_prog = ("./data_preparation/ResFinder_analyser.py")
	path_to_merge_res_point = ("./data_preparation/merge_resfinder_pointfinder.py")
	path_to_merge_input_output = ("./data_preparation/merge_input_output_files.py")
	

	for currentArgument, currentValue in arguments:
		print(currentValue)
		if currentArgument in ("-p","--PathPoint"):
			path_to_point = str(currentValue)
		if currentArgument in ("-i", "--input"):
			input_path = str(currentValue)
		if currentArgument in ("-d", "--PointData"):
			point_data_path = str(currentValue)
		if currentArgument in ("-s", "--species"):
			species_name = str(currentValue)
		if currentArgument in ("-k", "kma"):
			kma_path = str(currentValue)
		if currentArgument in ("-r", "--ResFinder"):
			path_to_res = str(currentValue)
		if currentArgument in ("-y", "--data_y"):
			path_to_y = str(currentValue)
		if currentArgument in ("-n", "--nn_path"):
			path_to_trained_nn = str(currentValue)
		if currentArgument in ("-g", "--point_prog"):
			path_to_pointfinder_prog = str(currentValue)
		if currentArgument in ("-e", "--res_prog"):
			path_to_resfinder_prog = str(currentValue)
		if currentArgument in ("-R", "--merge_results"):
			path_to_merge_res_point = str(currentValue)
		if currentArgument in ("-I", "--merge_inout"):
			path_to_merge_input_output = str(currentValue)

		
	###Pointfinder, ResFinder and SPAdes###

	##sample names will be included into the study 
	
	##make a direction to store pointfinder and resfinder

	os.system("mkdir pointfinder")
	os.system("mkdir resfinder")

	for item in os.listdir("%s" % input_path):
		each = item.split("_")[0] ##file name should be the first word separated by the "_" in the file name
		os.system("mkdir ./pointfinder/%s" %each)
		os.system("python3 %s -i %s/%s -o ./pointfinder/%s -p %s -s %s -m kma -m_p %s -u" % (path_to_point, input_path, item, each, point_data_path, species_name, kma_path))
		os.system("sleep 1s")
		os.system("python %s -ifq %s/%s -o ./resfinder/%s -s %s -acq" % (path_to_res, input_path, item, each, species_name))
		os.system("sleep 1s")
	
	##point and resfinder files are generated.
	##now, the files need to be analysed
	
	###Analysing PointFinder results###
	
	if "binary_representation.py" in path_to_pointfinder_prog:

		os.system("python %s -p ./pointfinder" % path_to_pointfinder_prog)

	elif "scored_representation.py" in path_to_pointfinder_prog:
		
		if currentArgument in ("-m", "--mut"):
			os.system("python %s -p ./pointfinder -m" % path_to_pointfinder_prog)

		else:
			os.system("python %s -p ./pointfinder" % path_to_pointfinder_prog)

	elif "nucleotide_representation.py" in path_to_pointfinder_prog:

		os.system("python %s -p ./pointfinder" % path_to_pointfinder_prog)

	elif "amino_acid_representation.py" in path_to_pointfinder_prog:
		
		os.system("python %s -p ./pointfinder" % path_to_pointfinder_prog)
	else:
		os.system("python %s -p ./pointfinder" % path_to_pointfinder_prog)


	###Analysing ResFinder results###
	##Find the acquired genes

	os.system("python %s -r ./resfinder" % path_to_resfinder_prog)
	
	###Merging ResFinder and PointFinder results 

	os.system("python %s -p ./pointfinder_chr_mutations.txt -r ./resfinder_acquired_genes.txt" % path_to_merge_res_point)

	###Matching input and output results 

	os.system("python %s -i ./output_res_point.txt -o %s" % (path_to_merge_input_output, path_to_y))

	###Run machine learning model
	
	###Run the NN model

	for currentArgument, currentValue in arguments:
		if currentArgument in ("-c","--distinct_database"):
			os.system("python %s -x data_x.txt -y data_y.txt -d" % path_to_trained_nn)

	for currentArgument, currentValue in arguments:
		if currentArgument in ("-m","--merged_database"):
			os.system("python %s -x data_x.txt -y data_y.txt -m" % path_to_trained_nn)

if __name__ == '__main__':
    main()