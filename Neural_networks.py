#!/usr/bin/python2.7
##import the required packages 
import getopt, sys
import warnings

if not sys.warnoptions:
    warnings.simplefilter("ignore")

##read commandline arguments first 
fullCmdArguments = sys.argv
##further argumnets 
argumentList = fullCmdArguments[1:]
#print(argumentList)

unixOptions = "x:y:z:c:s:r:e:d:l:nh"
gnuOptions = ["xdata=", "ydata=", "names=", "clusters=", "split=", "random=", "epochs=", "hidden=", "learning=", "normal","help"]


try:
	arguments, values = getopt.getopt(argumentList, unixOptions, gnuOptions)
except getopt.error as err:
	print(str(err))
	sys.exit(2)

for currentArgument, currentValue in arguments:
	if currentArgument in ("-h", "--help"):
		print("-x --xdata = input x data")
		print("-y --ydata = output y data") 
		print("-z --names = sample names")
		print("-c --cluster = path to the sample clusters")
		print("-s --split = number of CV splits")
		print("-r --random = random state")
		print("-n --normal = normalize the data")
		print("-l --learning = learning rate")
		print("-e --epochs = number of iterations")
		print("-d --hidden = number of neurons in the hidden layer")
		print("-h --help = show the help message (have fun!))")
		sys.exit()


def main():
	import matplotlib
	matplotlib.use('Agg')
	import torch
	import torch.nn as nn
	import numpy as np
	import torch.optim as optim
	from torch.autograd import Variable
	from sklearn.model_selection import KFold
	from sklearn.metrics import matthews_corrcoef
	import pandas
	import matplotlib.pyplot as plt
	from sklearn.metrics import roc_curve, auc
	from scipy import interp
	import collections
	import random
	from torch.utils import data
	from sklearn import utils
	from sklearn import preprocessing

	class _classifier(nn.Module):
		def __init__(self, nlabel):
			super(_classifier, self).__init__()
			self.main = nn.Sequential(
				nn.Linear(D_in, H),
				nn.ReLU(),
				nn.Linear(H, nlabel),
			)

		def forward(self, input):
			return self.main(input)

	for currentArgument, currentValue in arguments:
		if currentArgument in ("-x","--xdata"):
			data_x_all = (np.loadtxt("%s" % currentValue, dtype="float"))
		if currentArgument in ("-y","--ydata"):
			data_y_all = (np.loadtxt("%s" % currentValue))
		if currentArgument in ("-z", "--names"):
			names_open = open("%s" % currentValue, "r")
			names_read = names_open.readlines()
		if currentArgument in ("-c", "--clusters"):
			output_file_open = open("%s" % currentValue, "r")
			output_file = output_file_open.readlines()
		if currentArgument in ("-s", "--split"):
			cv = int(currentValue)
		if currentArgument in ("-r", "--random"):
			Random_State = int(currentValue)
		if currentArgument in ("-e", "--epochs"):
			n_epochs = int(currentValue)
		if currentArgument in ("-d", "--hidden"):
			n_hidden = int(currentValue)
		if currentArgument in ("-l", "--learning"):
			learning_rate = float(currentValue)


	##prepare a dictionary for clusters, the keys are cluster numbers, items are sample names. 

	dict_cluster = collections.defaultdict(list)

	for each in output_file:
		splitted = each.split()
		if splitted != []:
			if splitted[0].isdigit():
				dict_cluster[str(int(splitted[0])-1)].append(splitted[3])
			if splitted[0] == "Similar":
				splitted = each.split()
				splitted_2 = each.split(":")
				dict_cluster[str(int(splitted_2[1].split()[0])-1)].append(splitted[6])

	keys = sorted(dict_cluster.keys())


	f_n = data_y_all[0].size ##number of features 

	##correct the sample names
	names_all = []
	for each in range(len(names_read)):
		names_all.append(names_read[each].replace("\n", ""))

	##seperate some of the samples as validation data
	##needs to be done at the very beginning 

	all_samples = range(0, len(data_y_all))

	desired_keys = range(1, len(keys)+1, 6)
		
	##find the indexes of validation samples
	validation = []
	for each in desired_keys:
		for item in dict_cluster[str(each)]:
			if item in names_all:
				validation.append(names_all.index(item))

	##indexes of remaining samples 
	remained = list(set(all_samples) - set(validation))

	##validation data
	validation_x = data_x_all[validation]
	validation_y = data_y_all[validation]

	##if normalization is required 
	for currentArgument, currentValue in arguments:
		if currentArgument in ("-n", "--normal"):
			validation_x_raw = validation_x

	##remaining data
	data_x = data_x_all[remained]
	data_y = data_y_all[remained]

	##subtract the samples from the names as well:
	names = [] ##sample names for training and testing 
	for e in range(0, len(names_all)):
		if e not in validation:
			names.append(names_all[e])

	validation_names = list(set(names_all)-set(names)) ##validation sample names 	

	###construct the Artificial Neural Networks Model###
	##The feed forward NN has only one hidden layer 
	##The activation function used in the input and hidden layer is ReLU, in the output layer the sigmoid function. 
			
	H = n_hidden ###number of neurons in the hidden layer 

	D_in = len(data_x[0]) ##number of neurons in the input layer 
	N = len(data_x) ##number of input samples
	nlabel = data_y[0].size ## number of neurons in the output layer 
	epochs = n_epochs ## number of iterations the each model will be trained

	##prepare data stores##
	all_mcc_values = [] ##MCC results for the test data
	all_mcc_values_2 = [] ##MCC results for the validation data 

	pred_val_all = [] ##probabilities are for the validation set
	pred_test_res_all = [] ##probabilities for the test set

	aucs_all = [] ##all AUC values for the test data 
	tprs_all = [] ## all True Positives for the test data 

	mean_fpr = np.linspace(0, 1, 100)

	##Custom k fold cross validation
	##cross validation method divides the clusters and adds to the partitions. 
	##Samples are not divided directly due to sample similarity issue
	all_data_splits_pre = []
	all_available_data = range(0,len(dict_cluster)) ## all the clusters had
	clusters_n = len(dict_cluster) ##number of clusters 

	all_samples = [] ## all the samples had in the clusters
	for i in dict_cluster:
		for each in dict_cluster[i]:
			all_samples.append(each)

	##Shuffle the clusters and divide them
	shuffled = list(utils.shuffle(dict_cluster.keys(), random_state = Random_State)) ##shuffled cluster names 

	##Divide the clusters equally 
	r = int(len(shuffled)/cv) ## batches 

	a = 0
	b = r
	for i in range(cv):

		all_data_splits_pre.append(shuffled[a:b])
		
		if i != cv -2:
			a = b
			b = b + r
		
		else:
			a = b
			b = len(shuffled)

	##Extract the samples inside the clusters 
	##If the number of samples are lower than the expected number of samples per partition, get an extra cluster
	all_data_splits = []
	totals = []
	all_extra = []
	for i in range(len(all_data_splits_pre)):
		if i == 0:
			m = i + 1
		else:
			m = np.argmax(totals)
		tem = []
		extracted = list(set(all_data_splits_pre[i])-set(all_extra))

		for e in extracted:	
			elements = dict_cluster[str(e)]
			tem.append(len(elements))
		sum_tem = sum(tem)
		
		if sum_tem + 200 < len(all_samples)/float(cv):
			a = 0
			while sum_tem + 200 < len(all_samples)/float(cv):
				extra = list(utils.shuffle(all_data_splits_pre[m], random_state = Random_State))[a]
				extracted = extracted + [extra]
				all_extra.append(extra)
				a = a + 1
				tem = []
				for e in extracted:	
					elements = dict_cluster[str(e)]
					tem.append(len(elements))
				sum_tem = sum(tem)
				for item in range(len(all_data_splits)):
					all_data_splits[item] = list(set(all_data_splits[item]) - set([extra]))
		totals.append(sum_tem)	
		all_data_splits.append(extracted)
					
	m = nn.Sigmoid() ##sigmoid function for the ooutput layer
	##cross validation loop where the training and testing performed. 
	for cc in range(cv):
		test_clusters = all_data_splits[cc] ## clusters including test data 
		remain = list(set(range(cv)) - set([cc])) ## clusters including validation data 
		training_clusters = []
		for e in remain:
			for ee in all_data_splits[e]:
				training_clusters.append(ee)



		###extract the training data indexes:
		train_samples = []
		for each in training_clusters:
			for element in dict_cluster["%s" %each]:
				if element in names:
					train_samples.append(names.index(element))
				if element in names_all:
					ind = names_all.index(element)

		
		###extract the test data indexes:
		test_samples = []
		for each in test_clusters:
			for element in dict_cluster["%s" %each]:
				if element in names:
					test_samples.append(names.index(element))
				if element in names_all:
					ind = names_all.index(element)
					
		##training and test samples 			
		x_train, x_test = data_x[train_samples], data_x[test_samples]
		y_train, y_test = data_y[train_samples], data_y[test_samples]	

		##if user chose to normalize the data 
		for currentArgument, currentValue in arguments:
			if currentArgument in ("-n", "--normal"):
				scaler = preprocessing.StandardScaler().fit(x_train)
				x_train = scaler.transform(x_train)
				x_test = scaler.transform(x_test) ##scale the test data based on the training data 
				validation_x = scaler.transform(validation_x_raw) ##scale the validation data based on the training data

		##generate a NN model 
		classifier = _classifier(nlabel)
		##generate the optimizer - Stochastic Gradient Descent
		##learning rate = 0.001
		optimizer = optim.SGD(classifier.parameters(), lr = learning_rate)

		##iterate the loop for number of epochs 
		for epoc in range(epochs):
			x_train_new = torch.utils.data.TensorDataset(torch.from_numpy(x_train).float())
			y_train_new = torch.utils.data.TensorDataset(torch.from_numpy(y_train).float())
			all_data = zip(x_train_new, y_train_new)
			
			## the model is trained for 100 batches
			data_loader = torch.utils.data.DataLoader(all_data, batch_size=100, drop_last = False)
			
			losses = [] ## save the error for each iteration
			for i, (sample_x, sample_y) in enumerate(data_loader):
					inputv = sample_x[0]
					inputv = Variable(torch.FloatTensor(inputv)).view(len(inputv),-1)
					
					if f_n == 1:
						labelsv = sample_y[0]
					else:
						labelsv = sample_y[0][:,:]
					weights = labelsv.data.clone()
					
					##That step is added to handle missing outputs. 	
					##Weights are not updated in the presence of missing values. 
					
					weights[weights == 1.0] = 1
					weights[weights == 0.0] = 1
					weights[weights < 0] = 0
					
					##Calculate the loss/error using Binary Cross Entropy Loss 				

					criterion = nn.BCELoss(weight= weights, reduction= "none")
					output = classifier(inputv)
					loss = criterion(m(output), labelsv)
					loss = loss.mean() #compute loss
					optimizer.zero_grad() #zero gradients #previous gradients do not keep accumulating 
					loss.backward() #backpropagation
					optimizer.step() #weights updated 
					losses.append(loss.data.mean())
			#print('[%d/%d] Loss: %.3f' % (epoc+1, epochs, np.mean(losses))) ## print the loss per iteration 

		##apply the trained model to the validation data 
		pred_val = []
		for v, v_sample in enumerate(validation_x):
			val = Variable(torch.FloatTensor(v_sample)).view(1,-1)
			output_test = classifier(val) 
			out = m(output_test) 
			temp = []
			for h in out[0]:
				temp.append(float(h))
			pred_val.append(temp)
		pred_val = np.array(pred_val)
		pred_val_all.append(pred_val)
		
		##apply the trained model to the test data 
		pred_test_res = []
		for a, a_sample in enumerate(x_test):
			tested = Variable(torch.FloatTensor(a_sample)).view(1,-1)
			output_test = classifier(tested)
			out = m(output_test)
			temp = []
			for h in out[0]:
				temp.append(float(h))
			pred_test_res.append(temp)
		pred_test_res = np.array(pred_test_res)
		pred_test_res_all.append(pred_test_res)


		##Calculate MCC for thresholds from 0 to 1. 
		all_thresholds = [] ## MCC values for test data 
		all_thresholds_2 = [] ## MCC values for training data 
		for threshold in np.arange(0, 1.1, 0.1):
					###predictions for the test data
					pred = []
					mcc_all = []

					for x, x_sample in enumerate(x_test):
						test = Variable(torch.FloatTensor(x_sample)).view(1,-1)
						output_test = classifier(test)
						out = m(output_test)
						temp = [] 
						for c in out[0]:
							if c > threshold:
								temp.append(1)
							else:
								temp.append(0)
			
						pred.append(temp)
					y_test = np.array(y_test)
					pred = np.array(pred)
					
					##predictions for the training data
					pred_2 = []
					mcc_all_2 = []
					for x, x_sample in enumerate(x_train):
						train = Variable(torch.FloatTensor(x_sample)).view(1,-1)
						output_test = classifier(train)
						out = m(output_test)
						temp = [] 
						for c in out[0]:
							if c > threshold:
								temp.append(1)
							else:
								temp.append(0)
			
						pred_2.append(temp)
					y_train = np.array(y_train)
					pred_2 = np.array(pred_2)

					##mcc values for test and train data
					##exclude missing outputs 
					for i in range(f_n): 
						comp = []
						for t in range(len(y_train)):
 							if f_n == 1:
								if -1 != y_train[t]:
									comp.append(t)
							else:
								if -1 != y_train[t][i]:
									comp.append(t)
						y_train_sub = y_train[comp]
						pred_sub_2 = pred_2[comp]
						
						comp2 = []
						for t in range(len(y_test)):
							if f_n == 1:
								if -1 != y_test[t]:
									comp2.append(t)
							else:	
								if -1 != y_test[t][i]:
									comp2.append(t)
						y_test_sub = y_test[comp2]
						pred_sub = pred[comp2]
						if f_n == 1:
							mcc = matthews_corrcoef(y_test_sub, pred_sub)
							mcc_2 = matthews_corrcoef(y_train_sub, pred_sub_2)
						else:
							mcc = matthews_corrcoef(y_test_sub[:,i], pred_sub[:,i])
							mcc_2 = matthews_corrcoef(y_train_sub[:,i], pred_sub_2[:,i])
						#print(mcc)
						mcc_all.append(mcc)
						mcc_all_2.append(mcc_2)
					all_thresholds.append(mcc_all)
					all_thresholds_2.append(mcc_all_2)
		all_mcc_values.append([all_thresholds])
		all_mcc_values_2.append(all_thresholds_2)

		##Calculate the AUC values
		##Exclude missing values
		aucs = []
		tprs = []
		for a in range(f_n): 
			comp_auc = []
			for t in range(len(y_test)):
				if f_n == 1:
					if -1 != y_test[t]:
						comp_auc.append(t)
				else:
					if -1 != y_test[t][a]:
						comp_auc.append(t)
			y_test_auc = y_test[comp_auc]
			pred_auc = pred_test_res[comp_auc]

			fpr = dict()
			tpr = dict()
			roc_auc = dict()

			#Compute micro-average ROC curve and ROC area
			if f_n ==1 :
				fpr, tpr, _ = roc_curve(y_test_auc, pred_auc, pos_label = 1)
			else:
				fpr, tpr, _ = roc_curve(y_test_auc[:,a], pred_auc[:,a], pos_label = 1)
			tprs.append(interp(mean_fpr, fpr, tpr))
			tprs[-1][0] = 0.0
			roc_auc = auc(fpr, tpr)
			aucs.append(roc_auc)
			#plt.plot(fpr, tpr, color='darkorange', alpha = 0.3,lw=1)
		aucs_all.append(aucs)
		tprs_all.append(tprs)

	#####Generate MCC plots#####

	##Plot the average of the MCC results. 

	all_ant1 = []
	for i in range(f_n):
		cv_list = []
		for c in all_mcc_values:
			all_ant = []
			for each in c:
				each = np.array(each)
				if f_n == 1:
					all_ant.append(each)
				else:
					all_ant.append(each[:,i])
			cv_list.append(all_ant)
		all_ant1.append((np.sum(cv_list, axis = 0))[0])
					

	colors = ["darkblue", "darkred", "darkgreen", "orange", "purple", "magenta"]*100
	legends = range(1, 501)

	for i in range(f_n):
		aver_all_mcc = []
		for val in all_ant1[i]:
			aver_all_mcc.append(val/cv) ##average of the MCC results 
		plt.plot(np.arange(0,1.1,0.1),aver_all_mcc, color = colors[i], alpha = 1, label = str(legends[i]))
	plt.xlim([0,1])
	plt.xlabel("Thresholds")
	plt.ylabel("MCC average values for %s fold CV" % str(cv))
	plt.title("Thresholds vs MCC values")
	plt.legend(loc="best")
	plt.savefig("./MCC_output_test.png")
	plt.close()

	##Calculate the MCC for the validation data, for the same thresholds 
	all_thresholds_valid = []
	for threshold in np.arange(0, 1.1, 0.1):
		pred_validation = []
		for r in range(0, len(validation)):
			all_predictions = []
			for item in pred_val_all:
				all_predictions.append(item[r])
			all_predictions_all = np.array(all_predictions).sum(axis=0)/float(cv) #take the average of the probabilities
			temp = [] 
			for c in all_predictions_all:
				if c > threshold:
					temp.append(1)
				else:
					temp.append(0)
			
			pred_validation.append(temp)
		pred_validation = np.array(pred_validation)

		##mcc values for the validation data 
		##do not take into consideration missing outputs. 
		mcc_all = []
		for i in range(f_n): 
			comp = []
			for t in range(len(validation_y)): 
				if f_n == 1:
					if -1 != validation_y[t]:
						comp.append(t)
				else:
					if -1 != validation_y[t][i]:
						comp.append(t)
			y_val_sub = validation_y[comp]
			pred_sub_val = pred_validation[comp]
			if f_n == 1:
				mcc = matthews_corrcoef(y_val_sub, pred_sub_val)
			else:
				mcc = matthews_corrcoef(y_val_sub[:,i], pred_sub_val[:,i])
			mcc_all.append(mcc)
		all_thresholds_valid.append(mcc_all)

	##plot MCC values per output 
	for i in range(f_n):
		aver_all_mcc = []
		for val in all_thresholds_valid:
			if f_n == 1:
				aver_all_mcc.append(val)
			else:
				aver_all_mcc.append(val[i])
		plt.plot(np.arange(0,1.1,0.1),aver_all_mcc, color = colors[i], alpha = 1, label = legends[i])
	plt.xlim([0,1])
	plt.xlabel("Thresholds")
	plt.ylabel("MCC average values for %s fold CV" % str(cv))
	plt.title("Thresholds vs MCC values")
	plt.legend(loc="best")
	plt.savefig("./MCC_output_validation.png")
	plt.close()


	###plot the AUC values 
	plt.figure(figsize=(8, 8)) ##figure size 
	pred_val_all = np.sum(pred_val_all, axis = 0)/float(cv) ##take the average of the validation predictions

	##calculate the AUCs for the validation data
	##do not take into consideration missing outputs 
	for i in range(f_n):
		comp = []
		for t in range(len(validation_y)):
			if f_n == 1:
				if -1 != validation_y[t] or -1.0 != validation_y[t]:
					comp.append(t)
			else:
				if -1 != validation_y[t][i] or -1.0 != validation_y[t][i]:
					comp.append(t)
		y_val_sub = validation_y[comp]
		pred_sub_val = pred_val_all[comp]
		tprs = []
		aucs = []
		for t in tprs_all:
			tprs.append(t[i])
		for z in aucs_all: 
			aucs.append(z[i])	
		mean_tpr = np.mean(tprs, axis=0)
		mean_tpr[-1] = 1.0
		mean_auc = auc(mean_fpr, mean_tpr)
		std_auc = np.std(aucs)
		plt.plot(mean_fpr, mean_tpr, color=colors[i],
		 label=r'%s-Mean ROC (AUC = %0.2f $\pm$ %0.2f)' % (legends[i], mean_auc, std_auc), lw=2, alpha=.8)
		if f_n == 1:
			fpr1, tpr1, _ = roc_curve(y_val_sub, pred_sub_val, pos_label = 1)
		else:
			fpr1, tpr1, _ = roc_curve(y_val_sub[:,i], pred_sub_val[:,i], pos_label = 1)
		roc_auc1 = auc(fpr1, tpr1)
		plt.plot(fpr1, tpr1, color=colors[i], alpha = 1, lw=1, label='ROC curve for validation (area = %0.2f)' % roc_auc1)	

	plt.plot([0, 1], [0, 1], linestyle='--', lw=2, color='r', label='Chance', alpha=.8)
	plt.xlim([-0.05, 1.05])
	plt.ylim([-0.05, 1.05])
	plt.xlabel('False Positive Rate')
	plt.ylabel('True Positive Rate')
	plt.title("ROC Curve")
	plt.legend(loc="lower right")
	plt.savefig("./ROC_curve.png")
	plt.close()


if __name__ == '__main__':
    main()