#!/usr/bin/python2.7
##import the required packages 
import getopt, sys
import warnings

if not sys.warnoptions:
    warnings.simplefilter("ignore")

##read commandline arguments first 
fullCmdArguments = sys.argv
##further argumnets 
argumentList = fullCmdArguments[1:]
#print(argumentList)

unixOptions = "x:y:z:f:c:s:r:e:d:l:b:nh"
gnuOptions = ["xdata=", "ydata=", "names=", "features=","clusters=", "split=", "random=", "epochs=", "hidden=", "learning=", "background_n=", "normal","help"]


try:
	arguments, values = getopt.getopt(argumentList, unixOptions, gnuOptions)
except getopt.error as err:
	print(str(err))
	sys.exit(2)

for currentArgument, currentValue in arguments:
	if currentArgument in ("-h", "--help"):
		print("-x --xdata = input x data")
		print("-y --ydata = output y data") 
		print("-z --names = sample names")
		print("-f --features = data feature names")
		print("-c --cluster = path to the sample clusters")
		print("-s --split = number of CV splits")
		print("-r --random = random state")
		print("-n --normal = normalize the data")
		print("-l --learning = learning rate")
		print("-e --epochs = number of iterations")
		print("-d --hidden = number of neurons in the hidden layer")
		print("-b --background_n = number of background samples")
		print("-h --help = show the help message (have fun!))")
		sys.exit()


def main():
	import torch
	import torch.nn as nn
	import numpy as np
	import torch.optim as optim
	from torch.autograd import Variable
	from sklearn.model_selection import KFold
	from sklearn.metrics import matthews_corrcoef
	import pandas
	from sklearn.metrics import roc_curve, auc
	from scipy import interp
	import collections
	import random
	from torch.utils import data
	from sklearn import utils
	from sklearn import preprocessing
	import shap

	class _classifier(nn.Module):
		def __init__(self, nlabel):
			super(_classifier, self).__init__()
			self.main = nn.Sequential(
				nn.Linear(D_in, H),
				nn.ReLU(),
				nn.Linear(H, nlabel),
			)

		def forward(self, input):
			return self.main(input)

	for currentArgument, currentValue in arguments:
		if currentArgument in ("-x","--xdata"):
			data_x_all = (np.loadtxt("%s" % currentValue, dtype="float"))
		if currentArgument in ("-y","--ydata"):
			data_y_all = (np.loadtxt("%s" % currentValue))
		if currentArgument in ("-z", "--names"):
			names_open = open("%s" % currentValue, "r")
			names_read = names_open.readlines()
		if currentArgument in ("-f", "--features"):
			features_read = np.loadtxt("%s" % currentValue, dtype = "string")
		if currentArgument in ("-c", "--clusters"):
			output_file_open = open("%s" % currentValue, "r")
			output_file = output_file_open.readlines()
		if currentArgument in ("-s", "--split"):
			cv = int(currentValue)
		if currentArgument in ("-r", "--random"):
			Random_State = int(currentValue)
		if currentArgument in ("-e", "--epochs"):
			n_epochs = int(currentValue)
		if currentArgument in ("-d", "--hidden"):
			n_hidden = int(currentValue)
		if currentArgument in ("-l", "--learning"):
			learning_rate = float(currentValue)
		if currentArgument in ("-b", "--background_n"):
			num_back = int(currentValue)
	
	np.random.seed(Random_State)
	torch.manual_seed(Random_State)


	print(features_read)
	print(len(features_read))

	##prepare a dictionary for clusters, the keys are cluster numbers, items are sample names. 

	dict_cluster = collections.defaultdict(list)

	for each in output_file:
		splitted = each.split()
		if splitted != []:
			if splitted[0].isdigit():
				dict_cluster[str(int(splitted[0])-1)].append(splitted[3])
			if splitted[0] == "Similar":
				splitted = each.split()
				splitted_2 = each.split(":")
				dict_cluster[str(int(splitted_2[1].split()[0])-1)].append(splitted[6])

	keys = sorted(dict_cluster.keys())

	f_n = data_y_all[0].size ##number of features 

	##correct the sample names
	names_all = []
	for each in range(len(names_read)):
		names_all.append(names_read[each].replace("\n", ""))

	##seperate some of the samples as validation data
	##needs to be done at the very beginning 

	all_samples = range(0, len(data_y_all))

	desired_keys = range(1, len(keys)+1, 6)
		
	##find the indexes of validation samples
	validation = []
	for each in desired_keys:
		for item in dict_cluster[str(each)]:
			if item in names_all:
				validation.append(names_all.index(item))

	##indexes of remaining samples 
	remained = list(set(all_samples) - set(validation))

	##validation data
	validation_x = data_x_all[validation]
	validation_y = data_y_all[validation]

	##if normalization is required 
	for currentArgument, currentValue in arguments:
		if currentArgument in ("-n", "--normal"):
			validation_x_raw = validation_x

	##remaining data
	data_x = data_x_all[remained]
	data_y = data_y_all[remained]

	##subtract the samples from the names as well:
	names = [] ##sample names for training and testing 
	for e in range(0, len(names_all)):
		if e not in validation:
			names.append(names_all[e])

	validation_names = list(set(names_all)-set(names)) ##validation sample names 	

	###construct the Artificial Neural Networks Model###
	##The feed forward NN has only one hidden layer 
	##The activation function used in the input and hidden layer is ReLU, in the output layer the sigmoid function. 
			
	H = n_hidden ###number of neurons in the hidden layer 

	D_in = len(data_x[0]) ##number of neurons in the input layer 
	N = len(data_x) ##number of input samples
	nlabel = data_y[0].size ## number of neurons in the output layer 
	epochs = n_epochs ## number of iterations the each model will be trained


	##Custom k fold cross validation
	##cross validation method divides the clusters and adds to the partitions. 
	##Samples are not divided directly due to sample similarity issue
	all_data_splits_pre = []
	all_available_data = range(0,len(dict_cluster)) ## all the clusters had
	clusters_n = len(dict_cluster) ##number of clusters 

	all_samples = [] ## all the samples had in the clusters
	for i in dict_cluster:
		for each in dict_cluster[i]:
			all_samples.append(each)

	##Shuffle the clusters and divide them
	shuffled = list(utils.shuffle(dict_cluster.keys(), random_state = Random_State)) ##shuffled cluster names 

	##Divide the clusters equally 
	r = int(len(shuffled)/cv) ## batches 

	a = 0
	b = r
	for i in range(cv):

		all_data_splits_pre.append(shuffled[a:b])
		
		if i != cv -2:
			a = b
			b = b + r
		
		else:
			a = b
			b = len(shuffled)

	##Extract the samples inside the clusters 
	##If the number of samples are lower than the expected number of samples per partition, get an extra cluster
	all_data_splits = []
	
	totals = []
	all_extra = []
	for i in range(len(all_data_splits_pre)):
		if i == 0:
			m = i + 1
		else:
			m = np.argmax(totals)
		tem = []
		extracted = list(set(all_data_splits_pre[i])-set(all_extra))

		for e in extracted:	
			elements = dict_cluster[str(e)]
			tem.append(len(elements))
		sum_tem = sum(tem)
		
		if sum_tem + 200 < len(all_samples)/float(cv):
			a = 0
			while sum_tem + 200 < len(all_samples)/float(cv):
				extra = list(utils.shuffle(all_data_splits_pre[m], random_state = Random_State))[a]
				extracted = extracted + [extra]
				all_extra.append(extra)
				a = a + 1
				tem = []
				for e in extracted:	
					elements = dict_cluster[str(e)]
					tem.append(len(elements))
				sum_tem = sum(tem)
				for item in range(len(all_data_splits)):
					all_data_splits[item] = list(set(all_data_splits[item]) - set([extra]))
		totals.append(sum_tem)	
		all_data_splits.append(extracted)
					
	m = nn.Sigmoid() ##sigmoid function for the ooutput layer

	feature_importance = []

	##cross validation loop where the training and testing performed. 
	for cc in range(cv):
		test_clusters = all_data_splits[cc] ## clusters including test data 
		remain = list(set(range(cv)) - set([cc])) ## clusters including validation data 
		training_clusters = []
		for e in remain:
			for ee in all_data_splits[e]:
				training_clusters.append(ee)



		###extract the training data indexes:
		train_samples = []
		for each in training_clusters:
			for element in dict_cluster["%s" %each]:
				if element in names:
					train_samples.append(names.index(element))
				if element in names_all:
					ind = names_all.index(element)

		
		###extract the test data indexes:
		test_samples = []
		for each in test_clusters:
			for element in dict_cluster["%s" %each]:
				if element in names:
					test_samples.append(names.index(element))
				if element in names_all:
					ind = names_all.index(element)
					
		##training and test samples 			
		x_train, x_test = data_x[train_samples], data_x[test_samples]
		y_train, y_test = data_y[train_samples], data_y[test_samples]	

		##if user chose to normalize the data 
		for currentArgument, currentValue in arguments:
			if currentArgument in ("-n", "--normal"):
				scaler = preprocessing.StandardScaler().fit(x_train)
				x_train = scaler.transform(x_train)
				x_test = scaler.transform(x_test) ##scale the test data based on the training data 
				validation_x = scaler.transform(validation_x_raw) ##scale the validation data based on the training data

		##generate a NN model 
		classifier = _classifier(nlabel)
		##generate the optimizer - Stochastic Gradient Descent
		##learning rate = 0.001
		optimizer = optim.SGD(classifier.parameters(), lr = learning_rate)
		
		##iterate the loop for number of epochs
		for epoc in range(epochs):
			x_train_new = torch.utils.data.TensorDataset(torch.from_numpy(x_train).float())
			y_train_new = torch.utils.data.TensorDataset(torch.from_numpy(y_train).float())
			all_data = zip(x_train_new, y_train_new)
			
			## the model is trained for 100 batches
			data_loader = torch.utils.data.DataLoader(all_data, batch_size=100, drop_last = False)
			
			losses = [] ## save the error for each iteration
			for i, (sample_x, sample_y) in enumerate(data_loader):
					inputv = sample_x[0]
					inputv = Variable(torch.FloatTensor(inputv)).view(len(inputv),-1)
					
					if f_n == 1:
						labelsv = sample_y[0]
					else:
						labelsv = sample_y[0][:,:]
					weights = labelsv.data.clone()
					
					##That step is added to handle missing outputs. 	
					##Weights are not updated in the presence of missing values. 
					
					weights[weights == 1.0] = 1
					weights[weights == 0.0] = 1
					weights[weights < 0] = 0
					
					##Calculate the loss/error using Binary Cross Entropy Loss 				

					criterion = nn.BCELoss(weight= weights, reduction= "none")
					output = classifier(inputv)
					loss = criterion(m(output), labelsv)
					loss = loss.mean() #compute loss
					optimizer.zero_grad() #zero gradients #previous gradients do not keep accumulating 
					loss.backward() #backpropagation
					optimizer.step() #weights updated 
					losses.append(loss.data.mean())


		##feature importance using shap

		background = x_train[np.random.choice(x_train.shape[0], num_back, replace = False)]	

		explained = shap.DeepExplainer(classifier, Variable(torch.from_numpy(background).float()))

		shap_values = explained.shap_values(Variable(torch.from_numpy(x_test).float()))

		summed_shap_values = np.sum(shap_values, axis = 0)

		feature_importance.append(list(np.argsort(summed_shap_values)[-20:]))

		print(feature_importance)

	feature_importance_flat = []
	for item in feature_importance:
		for each in item:
			feature_importance_flat.append(each)

	uniq_feature_importance = list(set(feature_importance_flat))

	for each in uniq_feature_importance:
		if feature_importance_flat.count(each) >= 3:
			print(features_read[each])

if __name__ == '__main__':
    main()

